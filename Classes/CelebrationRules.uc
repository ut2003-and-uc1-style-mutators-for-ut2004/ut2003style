//==============================================================================
// CelebrationRules.uc
// Celebration Rules!
// 2008, GreatEmerald
//==============================================================================

class CelebrationRules extends GameRules;

var Pawn P;

function bool CheckEndGame(PlayerReplicationInfo Winner, string Reason)
{
    local Controller C;

    Super.CheckEndGame(Winner,Reason);
    for (C = Level.ControllerList; C != None; C = C.NextController)
    {
        if (C.PlayerReplicationInfo == Winner && AIController(C) != None && C.Pawn != None)
        {
            P = C.Pawn;
            if (P != None)
            {
                P.AnimEnd(0);
                if (P.bWaitForAnim)
                    P.bWaitForAnim=False;
                P.StopAnimating();
            }
        }
    }

    SetTimer(3.0, false);
    return true;
}

simulated function Timer()
{
    //local name MyCelebration;

    if (P != None && xPawn(P) != None)
    {
        P.PlayVictoryAnimation();
        /*MyCelebration = P.TauntAnims[4 + Rand(P.TauntAnims.Length - 3)];
        P.PlayAnim(MyCelebration,,0.1);*/
    }

}

defaultproperties
{
}
