//-----------------------------------------------------------------------------
// MutUT2003Weapons.uc
// Thanks to Wormbo for this code! :)
// Made by Wormbo and GreatEmerald, 2008
//-----------------------------------------------------------------------------

class MutUT2003Weapons extends Mutator;


/**
Modifies pickup bases to spawn the corresponding UT2003-style pickups.
*/
function bool CheckReplacement(Actor Other, out byte bSuperRelevant)
{
	local int i;
	local class<Pickup> NewPickupClass;
	local class<Weapon> NewWeaponClass;
	local WeaponLocker Locker;

	if (xWeaponBase(Other) != None) {
		NewWeaponClass = GetReplacementWeapon(xWeaponBase(Other).WeaponType);
		if (NewWeaponClass != None)
			xWeaponBase(Other).WeaponType = NewWeaponClass;
	}
	else if (WildcardBase(Other) != None) {

	}
	else if (xPickupBase(Other) != None) {
		NewPickupClass = GetReplacementPickup(xPickupBase(Other).Powerup);
		if (NewPickupClass != None)
			xPickupBase(Other).Powerup = NewPickupClass;
	}
	else if (WeaponLocker(Other) != None) {
		Locker = WeaponLocker(Other);

		for (i = 0; i < Locker.Weapons.Length; ++i) {
			NewWeaponClass = GetReplacementWeapon(Locker.Weapons[i].WeaponClass);
			if (NewWeaponClass != None)
				Locker.Weapons[i].WeaponClass = NewWeaponClass;
		}
	}
	else if (Pickup(Other) != None && Pickup(Other).MyMarker != None) {
		NewPickupClass = GetReplacementPickup(Pickup(Other).Class);
		if (NewPickupClass != None && ReplaceWith(Other, string(NewPickupClass))) {
			return false;
		}
	}
	return Super.CheckReplacement(Other, bSuperRelevant);
}


function string GetInventoryClassOverride(string InventoryClassName)
{
	local class<Weapon> NewWeaponClass;

	NewWeaponClass = GetReplacementWeapon(InventoryClassName);
	if (NewWeaponClass != None) {
		return string(NewWeaponClass);
	}
	return Super.GetInventoryClassOverride(InventoryClassName);
}


function class<Weapon> GetReplacementWeapon(coerce string Original)
{
	if (Right(Original, 6) ~= "Pickup")
		Original = Left(Original, Len(Original) - 6);
	switch (Locs(Original)) {
	case "xweapons.shockrifle":
		return class'UT2003ShockRifle';
	case "onslaught.onsavril":
		return class'UT2003AVRiL';
	case "onslaught.onsgrenadelauncher":
		return class'UT2003GrenadeLauncher';
	case "onslaught.onsminelayer":
		return class'UT2003MineLayer';
	case "onslaughtfull.onspainter":
		return class'UT2003ONSPainter';
	case "xweapons.redeemer":
		return class'UT2003Redeemer';
	case "xweapons.painter":
		return class'UT2003Painter';
	case "xweapons.supershockrifle":
	    return class'UT2003SuperShockRifle';
	case "xweapons.linkgun":
		return class'UT2003LinkGun';
	case "xweapons.biorifle":
		return class'UT2003BioRifle';
	case "xweapons.assaultrifle":
		return class'UT2003AssaultRifle';
	case "xweapons.minigun":
		return class'UT2003Minigun';
	case "xweapons.flakcannon":
		return class'UT2003FlakCannon';
	case "xweapons.rocketlauncher":
		return class'UT2003RocketLauncher';
	case "xweapons.sniperrifle":
	case "utclassic.classicsniperrifle":
		return class'UT2003SniperRifle';
	case "xweapons.translauncher":
		return class'UT2003Translocator';
	case "xweapons.shieldgun":
		return class'UT2003ShieldGun';
	default: return none;
	}
}


function class<Pickup> GetReplacementPickup(class<Pickup> Original)
{
	switch (Original) {
	case class'ShockAmmoPickup':
	    return class'UT2003ShockAmmoPickup';
    case class'LinkAmmoPickup':
	    return class'UT2003LinkAmmoPickup';
    case class'BioAmmoPickup':
	    return class'UT2003BioAmmoPickup';
    case class'AssaultAmmoPickup':
	    return class'UT2003AssaultAmmoPickup';
    case class'MinigunAmmoPickup':
	    return class'UT2003MinigunAmmoPickup';
    case class'FlakAmmoPickup':
	    return class'UT2003FlakAmmoPickup';
    case class'RocketAmmoPickup':
	    return class'UT2003RocketAmmoPickup';
    case class'SniperAmmoPickup':
    case class'UTClassic.ClassicSniperAmmoPickup':
	    return class'UT2003SniperAmmoPickup';
    case class'UDamagePack':
		return class'UT2003UDamagePickup';
	case class'ONSAVRiLAmmoPickup':
	    return class'UT2003AVRiLAmmoPickup';
    case class'ONSGrenadeAmmoPickup':
	    return class'UT2003GrenadeAmmoPickup';
    case class'ONSMineAmmoPickup':
	    return class'UT2003MineAmmoPickup';
    case class'MiniHealthPack':
		return class'UT2003MiniHealthPack';
	case class'HealthPack':
		return class'UT2003HealthPack';
	case class'SuperHealthPack':
		return class'UT2003SuperHealthPack';
	case class'ShieldPack':
		return class'UT2003ShieldPack';
	case class'SuperShieldPack':
		return class'UT2003SuperShieldPack';
	case class'AdrenalinePickup':
		return class'UT2003AdrenalinePickup';
	default: return none;
	}
}


//=============================================================================
// Default values
//=============================================================================

defaultproperties
{
	FriendlyName = "[UT2003 Style] UT2003 Weapons"
	Description  = "Changes the UT2004 weapons back to the UT2003 style."
	GroupName    = "Arena"
}
