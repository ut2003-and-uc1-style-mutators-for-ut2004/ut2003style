//==============================================================================
// UT2003SuperShockRiflePickup.uc
// This isn't what you see every day.
// GreatEmerald, 2008
//==============================================================================

class UT2003SuperShockRiflePickup extends SuperShockRiflePickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003SuperShockRifle'
}
