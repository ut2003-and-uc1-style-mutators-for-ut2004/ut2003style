//==============================================================================
// UT2003RocketFire.uc
// Rocket Scientist!
// GreatEmerald, 2008
//==============================================================================

class UT2003RocketFire extends RocketFire;

defaultproperties
{
    AmmoClass=class'UT2003RocketAmmo'
    FireSound=Sound'UT2003Weapons.RocketLauncher.RocketLauncherFire'
}
