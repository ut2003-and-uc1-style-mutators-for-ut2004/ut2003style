//==============================================================================
// UT2003ShieldFire.uc
// Get a pistol, not the piston...
// GreatEmerald, 2008
//==============================================================================

class UT2003ShieldFire extends ShieldFire;

defaultproperties
{
    ShieldRange=120.0
    SelfDamageScale=0.5
    MinHoldtime=0.25
}
