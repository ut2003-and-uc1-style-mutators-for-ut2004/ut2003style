//==============================================================================
// UT2003TransBeacon.uc
// Sizzle.
// GreatEmerald, 2008, 2009
//==============================================================================

class UT2003TransBeacon extends TransBeacon;

defaultproperties
{
    Mesh=Mesh'Weapons.TransBeacon'
    DrawType=DT_Mesh
    DrawScale=1.5
    PrePivot=(X=0.0,Y=0.0,Z=-7.0)
    TransTrailClass=class'TransTrailBlue' //Using as blue
    TransFlareClass=class'TransFlareBlue'
    LightHue=160
}
