//==============================================================================
// UT2003ShockAttachment.uc
// Yes, copying is fun.
// 2008, GreatEmerald
//==============================================================================

class UT2003ShockAttachment extends ShockAttachment;

defaultproperties
{
  Mesh=mesh'Weapons.ShockRifle_3rd'
  RelativeLocation=(X=0,Y=0,Z=0)
  RelativeRotation=(Pitch=0)
  Skins[0]=WeaponSkins.Skins.ShockTex0
  Skins[1]=WeaponSkins.Skins.ShockTex0
}
