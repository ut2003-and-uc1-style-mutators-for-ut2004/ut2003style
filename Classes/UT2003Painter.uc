//==============================================================================
// UT2003Painter.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003Painter extends Painter;

defaultproperties
{
    ItemName="UT2003 Ion Painter"
    PickupClass=class'UT2003PainterPickup'
    //IconMaterial=Material'InterfaceContent.Hud.SkinA' //GEm: Commented as it's the same as lightning gun, unfortunately
    //IconCoords=(X1=322,Y1=372,X2=444,Y2=462)
}
