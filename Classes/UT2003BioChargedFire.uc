//==============================================================================
// UT2003BioChargedFire.uc
// ParentBlob!
// 2008, GreatEmerald
//==============================================================================

class UT2003BioChargedFire extends BioChargedFire;

function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
    local BioGlob Glob;

    GotoState('');

    if (GoopLoad == 0) return None;

    Glob = BioGlob(Weapon.Spawn(ProjectileClass,,, Start, Dir));
    if ( Glob != None )
    {
        Glob.Damage *= DamageAtten;
        Glob.SetGoopLevel(GoopLoad);
        Glob.AdjustSpeed();
    }
    GoopLoad = 0;
    if ( Weapon.AmmoAmount(ThisModeNum) <= 0 )
        Weapon.OutOfAmmo();
    return Glob;
}

defaultproperties
{
    AmmoClass=class'UT2003BioAmmo'
    FireSound=Sound'UT2003Weapons.BioRifle.BioRifleFire'
    ProjectileClass=Class'UT2003BioGlob'
}
