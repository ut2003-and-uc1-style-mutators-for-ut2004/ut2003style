//==============================================================================
// UT2003GrenadePickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003GrenadePickup extends ONSGrenadePickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003GrenadeLauncher'
    //PickupSound=Sound'WeaponSounds.BReload1'
    //TransientSoundVolume=0.45
}
