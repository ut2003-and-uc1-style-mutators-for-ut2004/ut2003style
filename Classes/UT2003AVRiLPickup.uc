//==============================================================================
// UT2003AVRiLPickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003AVRiLPickup extends ONSAVRiLPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003AVRiL'
    //PickupSound=Sound'WeaponSounds.BPickup2'
    //TransientSoundVolume=0.45
}
