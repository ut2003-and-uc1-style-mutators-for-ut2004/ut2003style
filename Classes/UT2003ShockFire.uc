//==============================================================================
// UT2003ShockFire.uc
// Laser Rifle...
// 2008, GreatEmerald
//==============================================================================

class UT2003ShockFire extends ShockBeamFire;

defaultproperties
{
    FireSound=Sound'UT2003Weapons.ShockRifle.ShockRifleFire'
    AmmoClass=class'UT2003ShockAmmo'
}
