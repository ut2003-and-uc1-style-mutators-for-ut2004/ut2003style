//==============================================================================
// UT2003RedeemerPickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003RedeemerPickup extends RedeemerPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003Redeemer'
}
