//==============================================================================
// UT2003AssaultFire.uc
// Faster!
// 2008, GreatEmerald
//==============================================================================

class UT2003AssaultFire extends AssaultFire;

defaultproperties
{
    AmmoClass=class'UT2003MinigunAmmo'
    FireRate=0.15
}
