//-----------------------------------------------------------------------------
// ComboSuperjump.uc
// The UT2003 Beta SuperJump combo.
// Made by Leo (T.C.K.)
//-----------------------------------------------------------------------------

class ComboSuperjump extends Combo;

#exec OBJ LOAD FILE=GameSounds.uax

var xEmitter LeftTrail, RightTrail;

function StartEffect(xPawn P)
{
    if (P.Role == ROLE_Authority)
    {
        LeftTrail = Spawn(class'SuperjumpThrust', P,, P.Location, P.Rotation);
        P.AttachToBone(LeftTrail, 'lfoot');

        RightTrail = Spawn(class'SuperjumpThrust', P,, P.Location, P.Rotation);
        P.AttachToBone(RightTrail, 'rfoot');
    }

    P.AirControl *= 2.0;
    P.JumpZ *= 2.2;
}

function StopEffect(xPawn P)
{
    P.AirControl = P.Default.AirControl;
    P.JumpZ = P.Default.JumpZ;
}

defaultproperties
{
     ExecMessage="Super Jump!"
     ActivateSound=Sound'AnnouncerMain.Super_jump'
     ComboAnnouncementName=Super_jump
     Duration=30.000000//Adrenaline Cost is obsolete in UT2004, therefore duration is increased... Might have an impact on the game tho
     keys(0)=1
     keys(1)=2
     keys(2)=1
     keys(3)=2
}