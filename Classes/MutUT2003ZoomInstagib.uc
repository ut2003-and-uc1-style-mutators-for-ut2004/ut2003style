//=============================================================================
// MutUT2003ZoomInstagib.uc
// I didn't! Yay.
// 2008, GreatEmerald
//=============================================================================

class MutUT2003ZoomInstagib extends MutUT2003InstaGib;

function bool MutatorIsAllowed()
{
    return true;
}

defaultproperties
{
    WeaponName=UT2003ZoomSuperShockRifle
    WeaponString="UT2003Style.UT2003ZoomSuperShockRifle"
    DefaultWeaponName="UT2003Style.UT2003ZoomSuperShockRifle"
    FriendlyName="[UT2003 Style] Zoom InstaGib"
    Description="Instagib with UT2003 Super Shock Rifles and zoom functionality."
}
