//==============================================================================
// UT2003MinigunAmmo.uc
// How does it work?
// GreatEmerald, 2008
//==============================================================================

class UT2003MinigunAmmo extends MinigunAmmo;

defaultproperties
{
    PickupClass=class'MinigunAmmoPickup'
    MaxAmmo=350
}
