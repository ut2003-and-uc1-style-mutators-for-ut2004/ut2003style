//==============================================================================
// UT2003SniperFire.uc
// Head shot!
// GreatEmerald, 2008
//==============================================================================

class UT2003SniperFire extends SniperFire;

defaultproperties
{
    AmmoClass=class'UT2003SniperAmmo'
    FireRate=1.8
    NumArcs=4
    FireAnimRate=1.11
    FireSound=Sound'UT2003Weapons.LightningGun.LightningGunFire'
}
