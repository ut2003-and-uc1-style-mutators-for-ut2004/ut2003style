//==============================================================================
// UT2003ShieldAltFire.uc
// Stone skin...
// GreatEmerald, 2008
//==============================================================================

class UT2003ShieldAltFire extends ShieldAltFire;

defaultproperties
{
    AmmoPerFire=5
    ChargingSound=Sound'UT2003Weapons.ShieldGun.ShieldNoise'
}
