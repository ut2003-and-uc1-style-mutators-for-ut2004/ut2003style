//==============================================================================
// UT2003BioAmmoPickup.uc
// Sludge!
// GreatEmerald, 2008
//==============================================================================

class UT2003BioAmmoPickup extends BioAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
   InventoryType=class'UT2003BioAmmo'
}
