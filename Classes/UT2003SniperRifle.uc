//==============================================================================
// UT2003SniperRifle.uc
// Head Hunter!
// GreatEmerald, 2008
//==============================================================================

class UT2003SniperRifle extends SniperRifle;

simulated event RenderOverlays( Canvas Canvas ) //We'll hope this works auto-magically
{
    local float tileScaleX;
    local float tileScaleY;
    local float bX;
    local float bY;
    local float fX;
    local float fY;
    local float chargeBar;

    local float tX;
    local float tY;

    local float barOrgX;
    local float barOrgY;
    local float barSizeX;
    local float barSizeY;

    if ( PlayerController(Instigator.Controller) == None )
    {
        Super.RenderOverlays(Canvas);
        zoomed=false;
        return;
    }

    if ( LastFOV > PlayerController(Instigator.Controller).DesiredFOV )
    {
        PlaySound(Sound'WeaponSounds.LightningGun.LightningZoomIn', SLOT_Misc,,,,,false);
    }
    else if ( LastFOV < PlayerController(Instigator.Controller).DesiredFOV )
    {
        PlaySound(Sound'WeaponSounds.LightningGun.LightningZoomOut', SLOT_Misc,,,,,false);
    }
    LastFOV = PlayerController(Instigator.Controller).DesiredFOV;

    if ( (PlayerController(Instigator.Controller).DesiredFOV == PlayerController(Instigator.Controller).DefaultFOV)
        || (Level.bClassicView && (PlayerController(Instigator.Controller).DesiredFOV == 90)) )
    {
        Super.RenderOverlays(Canvas);
        zoomed=false;
    }
    else
    {

        if ( FireMode[0].NextFireTime <= Level.TimeSeconds )
        {
            chargeBar = 1.0;
        }
        else
        {
            chargeBar = 1.0 - ((FireMode[0].NextFireTime-Level.TimeSeconds) / FireMode[0].FireRate);
        }

        tileScaleX = Canvas.SizeX / 640.0f;
        tileScaleY = Canvas.SizeY / 480.0f;

        bX = borderX * tileScaleX;
        bY = borderY * tileScaleY;
        fX = 2*focusX * tileScaleX;
        fY = 2*focusY * tileScaleX;

        tX = testX * tileScaleX;
        tY = testY * tileScaleX;

        barOrgX = RechargeOrigin.X * tileScaleX;
        barOrgY = RechargeOrigin.Y * tileScaleY;

        barSizeX = RechargeSize.X * tileScaleX;
        barSizeY = RechargeSize.Y * tileScaleY;

        SetZoomBlendColor(Canvas);

        Canvas.Style = 255;
        Canvas.SetPos(0,0);
        Canvas.DrawTile( Material'ZoomFB', Canvas.SizeX, Canvas.SizeY, 128, 128, 256, 256 ); // !! hardcoded size

        Canvas.DrawColor = FocusColor;
        Canvas.DrawColor.A = 255; // 255 was the original -asp. WTF??!?!?!
        Canvas.Style = ERenderStyle.STY_Alpha;

        Canvas.SetPos((Canvas.SizeX*0.5)-fX,(Canvas.SizeY*0.5)-fY);
        Canvas.DrawTile( Texture'SniperFocus', fX*2.0, fY*2.0, 0.0, 0.0, Texture'SniperFocus'.USize, Texture'SniperFocus'.VSize );

        fX = innerArrowsX * tileScaleX;
        fY = innerArrowsY * tileScaleY;

        Canvas.DrawColor = ArrowColor;
        Canvas.SetPos((Canvas.SizeX*0.5)-fX,(Canvas.SizeY*0.5)-fY);
        Canvas.DrawTile( Texture'SniperArrows', fX*2.0, fY*2.0, 0.0, 0.0, Texture'SniperArrows'.USize, Texture'SniperArrows'.VSize );

        // Draw the Charging meter  -AsP
        Canvas.DrawColor = ChargeColor;
        Canvas.DrawColor.A = 255;

        if(chargeBar <1)
            Canvas.DrawColor.R = 255*chargeBar;
        else
        {
            Canvas.DrawColor.R = 0;
            Canvas.DrawColor.B = 0;
        }

        if(chargeBar == 1)
            Canvas.DrawColor.G = 255;
        else
            Canvas.DrawColor.G = 0;

        Canvas.Style = ERenderStyle.STY_Alpha;
        Canvas.SetPos( barOrgX, barOrgY );
        Canvas.DrawTile(Texture'Engine.WhiteTexture',barSizeX,barSizeY*chargeBar, 0.0, 0.0,Texture'Engine.WhiteTexture'.USize,Texture'Engine.WhiteTexture'.VSize*chargeBar);
        zoomed = true;
    }
}

defaultproperties
{
    ItemName="UT2003 Lightning Gun"
    FireModeClass(0)=UT2003SniperFire
    PickupClass=class'UT2003SniperRiflePickup'
    Skins(0)=Material'UT2003WeaponSkins.Skins.SniperTex'
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=209,Y1=257,X2=289,Y2=285)
}
