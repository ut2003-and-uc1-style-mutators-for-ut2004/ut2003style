//==============================================================================
// UT2003ZoomSuperShockBeamFire.uc
// How long can it get?
// GreatEmerald, 2008
//==============================================================================

class UT2003ZoomSuperShockBeamFire extends UT2003SuperShockBeamFire
config;

var config bool bAllowMultiHit;

function bool AllowMultiHit()
{
    return bAllowMultiHit;
}

defaultproperties
{
    bAllowMultiHit=true
    DamageType=class'ZoomSuperShockBeamDamage'
}
