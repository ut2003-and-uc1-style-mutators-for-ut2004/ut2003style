//==============================================================================
// UT2003MileLayerPickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003MineLayerPickup extends ONSMineLayerPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003MineLayer'
    //PickupSound=Sound'WeaponSounds.BReload7'
    //TransientSoundVolume=0.45
}
