//==============================================================================
// UT2003FlakCannonPickup.uc
// Spins...
// 2008, GreatEmerald
//==============================================================================

class UT2003FlakCannonPickup extends FlakCannonPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003FlakCannon'
}
