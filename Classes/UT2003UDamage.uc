//==============================================================================
// UT2003UDamagePickup.uc
// This might be hard...
// GreatEmerald, 2008
//==============================================================================

class UT2003UDamage extends Inventory;

var Sound UDamageFireSound;
var class<UDamageTimer> CustomTimer;

simulated function PostNetBeginPlay()
{
	local UDamageTimer UDT;
    
    if (xPawn(Instigator) != None)
    {
		xPawn(Instigator).UDamageSound = UDamageFireSound;
        //log(Self@"Pawn has UDamageTimer"@xPawn(Instigator).UDamageTimer);
        UDT = Spawn(CustomTimer, Instigator);
        xPawn(Instigator).UDamageTimer = UDT;
    }
    Super.PostNetBeginPlay();
}

defaultproperties
{
	// HACK: online UDamage fire sound change
	bAlwaysRelevant = True
	bOnlyRelevantToOwner = False
	bReplicateInstigator = True

	PickupClass = class'UT2003UDamagePickup'

	UDamageFireSound = Sound'UT2003Weapons.UDamage.UDamageFire'
	//WarningSound = Sound'UT2003Weapons.UDamage.UDamagePickUp'
    CustomTimer = Class'UT2003UDamageTimer'
}
