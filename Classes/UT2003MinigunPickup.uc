//==============================================================================
// UT2003MinigunPickup.uc
// Gattling...
// 2008, GreatEmerald
//==============================================================================

class UT2003MinigunPickup extends MinigunPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003Minigun'
}
