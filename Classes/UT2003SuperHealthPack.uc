//==============================================================================
// UT2003SuperHealthPack.uc
// Interesting sound.
// GreatEmerald, 2008
//==============================================================================

class UT2003SuperHealthPack extends SuperHealthPack;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
   PickupSound=sound'UT2003Weapons.Pickups.LargeHealthPickup'
   TransientSoundVolume=0.45
}
