//==============================================================================
// UT2003GrenadeAmmoPickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003GrenadeAmmoPickup extends ONSGrenadeAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003GrenadeLAmmo'
    //PickupSound=Sound'WeaponSounds.BReload11'
    //TransientSoundVolume=0.45
}
