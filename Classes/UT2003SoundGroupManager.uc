/*******************************************************************************
 * UT2003SoundGroupManager.uc
 * Simulates death sounds to match UT2003 feel without overwriting pawns
 * GreatEmerald, 2012
 ******************************************************************************/   

class UT2003SoundGroupManager extends Info
    NotPlaceable;

var Array<Sound> DeathSounds;

function PostBeginPlay()
{
    local xPawn xP;
    
    xP = xPawn(Owner); //GEm: Already checked that it's an xPawn on spawn
    if (xP.SoundGroupClass == None)
        Destroy(); //GEm: Nothing to manage, suicide
    
    DeathSounds = xP.SoundGroupClass.default.DeathSounds; //GEm: Copy the sounds
    xP.SoundGroupClass.default.DeathSounds.length = 0; //GEm: Delete the originals
}

defaultproperties
{
}
