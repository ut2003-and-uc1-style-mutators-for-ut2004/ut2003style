//==============================================================================
// UT2003AssaultAmmoPickup.uc
// Clip!
// GreatEmerald, 2008
//==============================================================================

class UT2003AssaultAmmoPickup extends AssaultAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003GrenadeAmmo'

    PickupMessage="You got a box of grenades."
}
