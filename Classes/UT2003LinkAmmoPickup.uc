//==============================================================================
// UT2003LinkAmmoPickup.uc
// Shiny x2!
// 2008, GreatEmerald
//==============================================================================

class UT2003LinkAmmoPickup extends LinkAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003LinkAmmo'
}
