//==============================================================================
// UT2003TransPickup.uc
// You won't get to see this every day.
// GreatEmerald, 2008
//==============================================================================

class UT2003TransPickup extends TransPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003Translocator'
    StaticMesh=StaticMesh'WeaponStaticMesh.TranslocatorNEW'
    Skins(0)=Shader'WeaponSkins.AmmoPickups.BioRifleGlassRef'
    Skins(1)=Shader'WeaponSkins.AmmoPickups.BioRifleGlassRef'
    Skins(2)=Texture'WeaponSkins.Skins.TransLauncherTex0'
    DrawScale=0.7
}
