//==============================================================================
// UT2003AVRiL.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003AVRiL extends ONSAVRiL;

defaultproperties
{
    ItemName="UT2003 AVRiL"
    FireModeClass(0)=UT2003AVRiLFire
    PickupClass=class'UT2003AVRiLPickup'
    //SelectSound=Sound'WeaponSounds.BImpactHammerStart'
    //TransientSoundVolume=0.45
}
