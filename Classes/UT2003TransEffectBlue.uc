/*******************************************************************************
 * UT2003TransEffectBlue.uc
 * For sound restoration!
 * GreatEmerald, 2012
 ******************************************************************************/   

class UT2003TransEffectBlue extends TransEffectBlue;

var Sound SpawnEffectSound;

simulated event PostBeginPlay()
{
    SetTimer(0.7,true);
    SetRotation(rot(0,0,0));
    PlaySound(SpawnEffectSound,SLOT_None);
    Super(xEmitter).PostBeginPlay();
}

defaultproperties
{
    SpawnEffectSound=Sound'UT2003Weapons.BaseGunTech.BWeaponSpawn1'
}
