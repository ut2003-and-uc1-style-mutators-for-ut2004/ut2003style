//=============================================================================
// MutUT2003Instagib.uc
// I must not forget the Zoom Instagib Rifle...
// 2008, GreatEmerald
//=============================================================================

class MutUT2003Instagib extends MutInstaGib;

var class<TransLauncher> TranslocatorClass;

function bool MutatorIsAllowed()
{
    return true;
}

function string GetInventoryClassOverride(string InventoryClassName)
{
    if (InventoryClassName ~= "XWeapons.TransLauncher")
        return string(TranslocatorClass);
    
    return Super.GetInventoryClassOverride(InventoryClassName);
}

defaultproperties
{
    AmmoName=SuperShockAmmo
    AmmoString="xWeapons.SuperShockAmmo"
    WeaponName=UT2003SuperShockRifle
    WeaponString="UT2003Style.UT2003SuperShockRifle"
    DefaultWeaponName="UT2003Style.UT2003SuperShockRifle"
    bAllowTranslocator=true
    TranslocatorClass=Class'UT2003Translocator'

    IconMaterialName="MutatorArt.nosym"
    ConfigMenuClassName=""
    GroupName="Arena"
    FriendlyName="[UT2003 Style] InstaGib"
    Description="Instagib with UT2003 Super Shock Rifles."
}
