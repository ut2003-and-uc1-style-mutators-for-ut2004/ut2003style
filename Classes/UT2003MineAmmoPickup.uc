//==============================================================================
// UT2003MineAmmoPickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003MineAmmoPickup extends ONSMineAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003MineAmmo'
    //PickupSound=Sound'WeaponSounds.BReload4'
    TransientSoundVolume=0.45
}
