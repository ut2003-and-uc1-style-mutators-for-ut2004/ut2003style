//==============================================================================
// UT2003RocketAmmoPickup.uc
// Rocket Scientist!
// GreatEmerald, 2008
//==============================================================================

class UT2003RocketAmmoPickup extends RocketAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003RocketAmmo'
}
