//==============================================================================
// UT2003AssaultGrenade.uc
// This is useful.
// GreatEmerald, 2008
//==============================================================================

class UT2003AssaultGrenade extends AssaultGrenade;

function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
    local Grenade g;
    local vector X, Y, Z;
    local float pawnSpeed;

    g = Weapon.Spawn(class'UT2003Grenade', instigator,, Start, Dir);
    if (g != None)
    {
        Weapon.GetViewAxes(X,Y,Z);
        pawnSpeed = X dot Instigator.Velocity;

        if ( Bot(Instigator.Controller) != None )
            g.Speed = mHoldSpeedMax;
        else
            g.Speed = mHoldSpeedMin + HoldTime*mHoldSpeedGainPerSec;
        g.Speed = FClamp(g.Speed, mHoldSpeedMin, mHoldSpeedMax);
        g.Speed = pawnSpeed + g.Speed;
        g.Velocity = g.Speed * Vector(Dir);

        g.Damage *= DamageAtten;
    }
    return g;
}



defaultproperties
{
    mWaitTime=0.3
    mHoldSpeedMin=850
    mHoldSpeedMax=1600
    mHoldSpeedGainPerSec=750
    mSpeedMin=250.f
    mSpeedMax=3000.f
    mScaleMultiplier=0.9f
    mScale=1.f
    mBlend=1.f
    mDrumRotationsPerSec=1.f
    FireAnimRate=1.0

    FireSound=Sound'UT2003Weapons.AssaultRifle.AssaultRifleAltFire'

    FireRate=0.5f
    MaxHoldTime=0.f
    AmmoClass=class'UT2003GrenadeAmmo'
}
