//-----------------------------------------------------------------------------
// UT2003LinkGun.uc
// A class to replace LinkGun for firing old projectiles.
// Made by GreatEmerald, 2008
//-----------------------------------------------------------------------------

class UT2003LinkGun extends LinkGun;

defaultproperties
{
    ItemName="UT2003 Link Gun"
    PickupClass=class'UT2003LinkGunPickup'
    FireModeClass(0)=UT2003LinkFire
    FireModeClass(1)=UT2003LinkAltFire
    AttachmentClass=class'UT2003LinkAttachment'
    bUseOldWeaponMesh=True
    SelectSound=Sound'WeaponSounds.LinkGun.SwitchToLinkGun'
    //IconMaterial=Material'InterfaceContent.Hud.SkinA'
    //IconCoords=(X1=200,Y1=190,X2=321,Y2=280)
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=130,Y1=123,X2=209,Y2=182)
}
