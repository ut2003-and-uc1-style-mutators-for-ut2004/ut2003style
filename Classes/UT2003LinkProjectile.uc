//-----------------------------------------------------------------------------
// UT2003LinkProjectile.uc
// The projectile with a nice trail instead of formless blob
// Made by GreatEmerald, 2008
//-----------------------------------------------------------------------------

class UT2003LinkProjectile extends LinkProjectile;

var xEmitter TrailUT2003;

simulated function Destroyed()
{
    if (TrailUT2003 != None)
    {
        TrailUT2003.Destroy();
    }
    Super.Destroyed();
}

simulated function LinkAdjust()
{
    if (Links > 0)
    {
        if ( TrailUT2003 != None )
        {
            TrailUT2003.Skins[0] = Texture'XEffectMat.link_muz_yellow';
        }
        /*if ( TrailUT2003 != None )
            TrailUT2003.MakeYellow();*/

        MaxSpeed = default.MaxSpeed + 350*Links;
        Skins[0] = FinalBlend'XEffectMat.LinkProjYellowFB';
        LightHue = 40;
    }
}

simulated function PostNetBeginPlay()
{
    //local float dist;
    local PlayerController PC;

    Acceleration = Normal(Velocity) * 3000.0;

    if ( (Level.NetMode != NM_DedicatedServer) && (Level.DetailMode != DM_Low) )
        TrailUT2003 = Spawn(class'xEffects.LinkProjEffect',self);
/*    if ( (TrailUT2003 != None) && (Instigator != None) && Instigator.IsLocallyControlled() )
    {
        if ( Role == ROLE_Authority )
            TrailUT2003.Delay(0.1);
        else
        {
            dist = VSize(Location - Instigator.Location);
            if ( dist < 100 )
                TrailUT2003.Delay(0.1 - dist/1000);
        }
    }*/

    if (Role < ROLE_Authority)
        LinkAdjust();
    if ( Level.NetMode == NM_DedicatedServer )
        return;
    if ( Level.bDropDetail || (Level.DetailMode == DM_Low) )
    {
        bDynamicLight = false;
        LightType = LT_None;
    }
    else
    {
        PC = Level.GetLocalPlayerController();
        if ( (PC == None) || (Instigator == None) || (PC != Instigator.Controller) )
        {
            bDynamicLight = false;
            LightType = LT_None;
        }
    }
}
