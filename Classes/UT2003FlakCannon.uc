//==============================================================================
// UT2003FlakCannon.uc
// Flak Attack!
// GreatEmerald, 2008
//==============================================================================

class UT2003FlakCannon extends FlakCannon;

defaultproperties
{
    ItemName="UT2003 Flak Cannon"
    FireModeClass(0)=UT2003FlakFire
    FireModeClass(1)=UT2003FlakAltFire
    PickupClass=class'UT2003FlakCannonPickup'
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=209,Y1=79,X2=289,Y2=116)
}
