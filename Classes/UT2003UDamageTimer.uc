/*******************************************************************************
 * UT2003UDamageTimer.uc
 * Abusing loopholes to deploy a counterhack, heck yea!
 * GreatEmerald, 2012
 ******************************************************************************/  

class UT2003UDamageTimer extends UDamageTimer; //GEm: Yes, we extend it just to make use of that loophole.

#exec OBJ LOAD FILE=UT2003Weapons.uax

var Sound UDamageSound; //GEm: Just to show how it was *meant* to be done! Take that!

function Timer()
{
    if ( Pawn(Owner) == None )
    {
        Destroy();
        return;
    }
    if ( SoundCount < 4 )
    {
        SoundCount++;
        Pawn(Owner).PlaySound(UDamageSound, SLOT_None, 1.5*Pawn(Owner).TransientSoundVolume,,1000,1.0);
        SetTimer(0.75,false);
        return;
    }
    Pawn(Owner).DisableUDamage();
    Destroy();
}

defaultproperties
{
    UDamageSound = Sound'UT2003Weapons.UDamagePickup'
}
