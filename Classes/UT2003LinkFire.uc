//-----------------------------------------------------------------------------
// UT2003LinkFire.uc
// A class to replace LinkGun Fire for firing old projectiles.
// Made by GreatEmerald, 2008
//-----------------------------------------------------------------------------

class UT2003LinkFire extends LinkAltFire;

function Projectile SpawnProjectile(Vector Start, Rotator Dir)
{
    local UT2003LinkProjectile UT2003Proj;

    Start += Vector(Dir) * 10.0 * LinkGun(Weapon).Links;
    UT2003Proj = Weapon.Spawn(class'UT2003LinkProjectile',,, Start, Dir);
    if ( UT2003Proj != None )
    {
        UT2003Proj.Links = LinkGun(Weapon).Links;
        UT2003Proj.LinkAdjust();
    }
    return UT2003Proj;
}


defaultproperties
{
    AmmoPerFire=1
    ProjectileClass=class'UT2003LinkProjectile'
    AmmoClass=class'UT2003LinkAmmo'
    FireSound=Sound'UT2003Weapons.LinkGun.PulseRifleFire'
    LinkedFireSound=Sound'UT2003Weapons.LinkGun.BLinkedFire'
}
