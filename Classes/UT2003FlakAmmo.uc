//==============================================================================
// UT2003FlakAmmo.uc
// Flak Monkey!
// GreatEmerald, 2008
//==============================================================================

class UT2003FlakAmmo extends FlakAmmo;

defaultproperties
{
    PickupClass=class'UT2003FlakAmmoPickup'
    MaxAmmo=50
}
