//==============================================================================
// UT2003AssaultAttachment.uc
// Detach...
// 2008, GreatEmerald
//==============================================================================

class UT2003AssaultAttachment extends AssaultAttachment;

defaultproperties
{
  Mesh=mesh'Weapons.AssaultRifle_3rd'
  RelativeLocation=(X=0,Y=0,Z=0)
  RelativeRotation=(Pitch=0)
}
