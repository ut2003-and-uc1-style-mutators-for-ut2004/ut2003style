//==============================================================================
// UT2003FlakFire.uc
// Flak Monkey!
// GreatEmerald, 2008
//==============================================================================

class UT2003FlakFire extends FlakFire;

defaultproperties
{
    AmmoClass=class'UT2003FlakAmmo'
    FireAnimRate=1.0
    FireSound=Sound'UT2003Weapons.FlakCannon.FlakCannonFire'
    FireRate=0.85
}
