//==============================================================================
// UT2003ShockAltFire.uc
// Nothing to see here.
// 2008, GreatEmerald
//==============================================================================

class UT2003ShockAltFire extends ShockProjFire;

defaultproperties
{
    FireSound=Sound'UT2003Weapons.ShockRifle.ShockRifleAltFire'
    AmmoClass=class'UT2003ShockAmmo'
}

