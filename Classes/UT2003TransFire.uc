//==============================================================================
// UT2003TransFire.uc
// Sizzle.
// GreatEmerald, 2008
//==============================================================================

class UT2003TransFire extends TransFire;

function projectile SpawnProjectile(Vector Start, Rotator Dir)
{
    local UT2003TransBeacon TransBeacon;

    if (UT2003Translocator(Weapon).TransBeacon == None)
    {
        if ((Instigator == None) || (Instigator.PlayerReplicationInfo == None) || (Instigator.PlayerReplicationInfo.Team == None) || ( Instigator.PlayerReplicationInfo.Team.TeamIndex == 0 ))
			TransBeacon = Weapon.Spawn(class'UT2003RedBeacon',,, Start, Dir); //Red one
		else
			TransBeacon = Weapon.Spawn(class'UT2003TransBeacon',,, Start, Dir);
        UT2003Translocator(Weapon).TransBeacon = TransBeacon;
        Weapon.PlaySound(TransFireSound,SLOT_Interact,,,,,false);
    }
    else
    {
        UT2003Translocator(Weapon).ViewPlayer();
        if ( UT2003Translocator(Weapon).TransBeacon.Disrupted() )
        {
            if( (Instigator != None) && (PlayerController(Instigator.Controller) != None) )
                PlayerController(Instigator.Controller).ClientPlaySound(Sound'WeaponSounds.BSeekLost1');
        }
        else
        {
            UT2003Translocator(Weapon).TransBeacon.Destroy();
            UT2003Translocator(Weapon).TransBeacon = None;
            Weapon.PlaySound(RecallFireSound,SLOT_Interact,,,,,false);
        }
    }
    return TransBeacon;
}

defaultproperties
{
    ProjectileClass=class'UT2003TransBeacon'
}
