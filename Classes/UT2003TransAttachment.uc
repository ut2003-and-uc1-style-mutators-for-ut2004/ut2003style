//==============================================================================
// UT2003TransAttachment.uc
// Sick!
// GreatEmerald, 2008
//==============================================================================

class UT2003TransAttachment extends TransAttachment;

simulated event BaseChange();

defaultproperties
{
    Mesh=mesh'Weapons.TransLauncher_3rd'
    Skins(0)=Texture'WeaponSkins.Skins.TransLauncherTex0'
    Skins(1)=Shader'WeaponSkins.AmmoPickups.BioRifleGlassRef'
    Skins(2)=Texture'WeaponSkins.Skins.TransLauncherTex0'
    Skins(3)=Texture'WeaponSkins.Skins.TransLauncherTex0'
}
