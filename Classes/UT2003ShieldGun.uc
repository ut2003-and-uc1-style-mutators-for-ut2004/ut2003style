//==============================================================================
// UT2003ShieldGun.uc
// Spiritual armour...
// GreatEmerald, 2008
//==============================================================================

class UT2003ShieldGun extends ShieldGun;

defaultproperties
{
    FireModeClass(1)=UT2003ShieldAltFire
    FireModeClass(0)=UT2003ShieldFire
    ItemName="UT2003 Shield Gun"
    PickupClass=class'UT2003ShieldGunPickup'
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    //IconCoords=(X1=130,Y1=182,X2=209,Y2=242)
    IconCoords=(X1=130,Y1=192,X2=209,Y2=232)
}
