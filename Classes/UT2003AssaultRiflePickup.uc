//==============================================================================
// UT2003AssaultRiflePickup.uc
// Sure, let's change this single setting in this class and overwrite it!
// GreatEmerald, 2008
//==============================================================================

class UT2003AssaultRiflePickup extends AssaultRiflePickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003AssaultRifle'
    StaticMesh=StaticMesh'WeaponStaticMesh.AssaultRiflePickup'
    Standup=(Z=0.75,Y=0,X=0)
}
