/*******************************************************************************
 * UT2003DeathPhraseRules.uc
 * Controls playing death phrases and death sounds.
 * GreatEmerald, 2012
 ******************************************************************************/    

class UT2003DeathPhraseRules extends GameRules;

var bool bGibbed;

function bool PreventDeath(Pawn Killed, Controller Killer, class<DamageType> damageType, vector HitLocation)
{
    
    if ( (NextGameRules != None) && NextGameRules.PreventDeath(Killed,Killer, damageType,HitLocation) )
        return true;
    
    //Level.GetLocalPlayerController().ClientMessage(Self@"PreventDeath!");
    if (MutUT2003Extra(Owner) != None && xPawn(Killed) != None)
        PlayDyingSound(xPawn(Killed), damageType, MutUT2003Extra(Owner));
    bGibbed = false;
    
    return false;
}

function bool PreventSever(Pawn Killed, Name boneName, int Damage, class<DamageType> DamageType)
{
    if ( (NextGameRules != None) && NextGameRules.PreventSever(Killed, boneName, Damage, DamageType) )
    {
        bGibbed = false; //GEm: Not strictly necessary, but makes logical sense.
        return true;
    }
    
    if (Killed.Health <= 0 && (boneName == 'head' || boneName == 'spine'))
    {
        bGibbed = true; //GEm: PreventSever() apparently gets called before PreventDeath() - silly, I know -
                        //GEm: and in the mean while there shouldn't be any other deaths, as the game's not multithreaded.
        //Level.GetLocalPlayerController().ClientMessage(Self@Killed@"("$Killed.OwnerName$") got gibbed!");
    }
    return false;
}

function PlayDyingSound(xPawn P, class<DamageType> DamageType, MutUT2003Extra Mut)
{
    local int i;
    local bool playedDeathPhrase;
    
    // Dont play dying sound if a skeleton. Tricky without vocal chords.
    if (DamageType.default.bSkeletize
        || P.HeadVolume.bWaterVolume
        || DamageType.default.bAlwaysGibs
        || bGibbed)
        return;
    
    P.SoundGroupClass.default.DeathSounds.length = 1; //GEm: Prevent death sounds from playing in the future
    P.SoundGroupClass.default.DeathSounds[0] = None; //GEm: Needs a dummy sound so that it doesn't logspam
    
    //log(Self@"Playing for class"@P.VoiceClass@"like"@P.VoiceClass.default.DeathPhrases[0]);
    
    if(FRand() < 0.08) //GEm: Originally 0.05, but in some cases no gibbing is treated as complete gibbing, so have to compensate
    {
        playedDeathPhrase = P.VoiceClass.static.PlayDeathPhrase(P);
        if(playedDeathPhrase)
            return;
    }
    
    for (i = 0; i<Mut.DeathSoundMaps.length; i++)
    {
        if (Mut.DeathSoundMaps[i].SoundGroupClass == P.SoundGroupClass)
        {
            PlaySound(Mut.DeathSoundMaps[i].DeathSounds[rand(Mut.DeathSoundMaps[i].DeathSounds.length)], SLOT_Pain, 2.5*TransientSoundVolume, true, 500);
            break;
        }
    }
}

defaultproperties
{
}
