//==============================================================================
// UT2003LinkAmmo.uc
// Sparks again!
// 2008, GreatEmerald
//==============================================================================

class UT2003LinkAmmo extends LinkAmmo;

defaultproperties
{
    PickupClass=class'UT2003LinkAmmoPickup'
    MaxAmmo=120
    InitialAmount=40
}
