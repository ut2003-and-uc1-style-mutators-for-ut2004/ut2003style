//==============================================================================
// UT2003RocketMultiFire.uc
// Rocket Scientist!
// GreatEmerald, 2008
//==============================================================================

class UT2003RocketMultiFire extends RocketMultiFire;

defaultproperties
{
    AmmoClass=class'UT2003RocketAmmo'
    FireSound=Sound'UT2003Weapons.RocketLauncher.RocketLauncherAltFire'
}
