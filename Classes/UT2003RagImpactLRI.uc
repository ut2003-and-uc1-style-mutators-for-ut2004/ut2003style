/*******************************************************************************
 * UT2003RagImpactLRI.uc
 * LinkedReplicationInfo for clients to simulate changing pawns' RagImpactSounds
 * array.
 * GreatEmerald, 2012
 ******************************************************************************/   

class UT2003RagImpactLRI extends LinkedReplicationInfo;

simulated function PostNetReceive()
{
    local int i;
    
    //log(Self@"PostNetReceive!");
    
    if (xPawn(Owner) != None) //GEm: The pawn is relevant!
    {
        log(Self@"Pawn is relevant!");
        for (i=0; i<4; i++)
            xPawn(Owner).RagImpactSounds[i] = Class'MutUT2003Extra'.default.UT2003RagImpactSounds[i];
    }
    Super.PostNetReceive();
}

defaultproperties
{
    bNetNotify=true
}
