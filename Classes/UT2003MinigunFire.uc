//==============================================================================
// UT2003MinigunFire.uc
// Holey!
// GreatEmerald, 2008
//==============================================================================

class UT2003MinigunFire extends MinigunFire;

function DoTrace(Vector Start, Rotator Dir)
{
    if ( FRand() < +1.0 )    //Lockdown!
        Momentum = FMax(1.0,+1.0) * Default.Momentum;
    else
        Momentum = 0;
    Super.DoTrace(Start,Dir);
}

defaultproperties
{
    Momentum=+1200.0
    DamageMin=6
    DamageMax=7
    FiringSound=Sound'UT2003Weapons.Minigun.minifireb'
    AmmoClass=class'UT2003MinigunAmmo'
}
