//==============================================================================
// UT2003BioRiflePickup.uc
// Bio Hazard!
// 2008, GreatEmerald
//==============================================================================

class UT2003BioRiflePickup extends BioRiflePickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003BioRifle'
    Standup=(Z=0.75,Y=0,X=0)
}

