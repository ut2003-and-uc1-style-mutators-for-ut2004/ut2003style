//-----------------------------------------------------------
// UT2003RedBeacon.uc
// This one's RED.
// 2009, GreatEmerald
//-----------------------------------------------------------
class UT2003RedBeacon extends UT2003TransBeacon;

DefaultProperties
{
    TransTrailClass=class'TransTrailRed'
    TransFlareClass=class'TransFlareRed'
    LightHue=0
}
