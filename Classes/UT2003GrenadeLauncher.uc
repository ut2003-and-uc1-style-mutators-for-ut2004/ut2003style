//==============================================================================
// UT2003GrenadeLauncher.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003GrenadeLauncher extends ONSGrenadeLauncher;

defaultproperties
{
    ItemName="UT2003 Grenade Launcher"
    FireModeClass(0)=UT2003GrenadeFire
    PickupClass=class'UT2003GrenadePickup'
    //SelectSound=Sound'WeaponSounds.BReload2'
    //TransientSoundVolume=0.45
}
