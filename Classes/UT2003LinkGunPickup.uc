//-----------------------------------------------------------------------------
// UT2003LinkGunPickup.uc
// A class to replace LinkGun Pickup for firing old projectiles.
// Made by GreatEmerald, 2008
//-----------------------------------------------------------------------------

class UT2003LinkGunPickup extends LinkGunPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003LinkGun'
    StaticMesh=StaticMesh'WeaponStaticMesh.LinkGunPickup'
    DrawScale=0.6
    Standup=(Z=0.75,Y=0,X=0)
}
