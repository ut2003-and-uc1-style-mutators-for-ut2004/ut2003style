//==============================================================================
// UT2003Minigun.uc
// Streak... Yellow streak?
// 2008, GreatEmerald
//==============================================================================

class UT2003Minigun extends Minigun;

/*function DropFrom(vector StartLocation)
{
    local Ammunition UT2003AssaultAmmo;
    local Weapon UT2003AssaultRifle;

    Super.DropFrom(StartLocation);
    if ( (Instigator != None) && (Instigator.Health > 0) )
    {
        // share ammo with assault rifle, so need to add it back to instigator's inventory
        UT2003AssaultAmmo = Ammunition(Instigator.FindInventoryType(class'UT2003MinigunAmmo'));
        if ( UT2003AssaultAmmo == None )
        {
            UT2003AssaultAmmo = spawn(class'UT2003MinigunAmmo',Instigator);
            UT2003AssaultAmmo.GiveTo( Instigator, None );
        }
        UT2003AssaultAmmo.AmmoAmount = 0;
        UT2003AssaultRifle = Weapon(Instigator.FindInventoryType(class'UT2003AssaultRifle'));
        if ( UT2003AssaultRifle != None )
            UT2003AssaultRifle.Ammo[0] = UT2003AssaultAmmo;
    }
} */

defaultproperties
{
    ItemName="UT2003 Minigun"
    FireModeClass(0)=UT2003MinigunFire
    FireModeClass(1)=UT2003MinigunAltFire
    PickupClass=class'UT2003MinigunPickup'
    bNoAmmoInstances=False
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    //IconCoords=(X1=130,Y1=241,X2=209,Y2=301)
    IconCoords=(X1=130,Y1=257,X2=209,Y2=285)
}
