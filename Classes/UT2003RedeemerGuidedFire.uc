/*******************************************************************************
 * UT2003RedeemerGuidedFire.uc
 * No really, Epic, why do you have to hardcode everything?!
 * GreatEmerald, 2012
 ******************************************************************************/

class UT2003RedeemerGuidedFire extends RedeemerGuidedFire;

var class<RedeemerWarhead> WarheadClass;
var class<Actor> ExplosionClass;
var class<DamageType> DamageTypeClass;

function Projectile SpawnProjectile(Vector Start, Rotator Dir)
{
    local RedeemerWarhead Warhead;
    local PlayerController Possessor;
    
    Warhead = Weapon.Spawn(WarheadClass, Instigator,, Start, Dir);
    if (Warhead == None)
        Warhead = Weapon.Spawn(WarheadClass, Instigator,, Instigator.Location, Dir);
    if (Warhead != None)
    {
        Warhead.OldPawn = Instigator;
        Warhead.PlaySound(FireSound);
        Possessor = PlayerController(Instigator.Controller);
        Possessor.bAltFire = 0;
        if ( Possessor != None )
        {
            if ( Instigator.InCurrentCombo() )
                Possessor.Adrenaline = 0;
            Possessor.UnPossess();
            Instigator.SetOwner(Possessor);
            Instigator.PlayerReplicationInfo = Possessor.PlayerReplicationInfo;
            Possessor.Possess(Warhead);
        }
        Warhead.Velocity = Warhead.AirSpeed * Vector(Warhead.Rotation);
        Warhead.Acceleration = Warhead.Velocity;
        WarHead.MyTeam = Possessor.PlayerReplicationInfo.Team;
    }
    else 
    {
        Weapon.Spawn(ExplosionClass);    
        Weapon.HurtRadius(500, 400, DamageTypeClass, 100000, Instigator.Location);
    }

    bIsFiring = false;
    StopFiring();
    return None;
}

defaultproperties
{
    WarheadClass=Class'UT2003RedeemerWarhead'
    ExplosionClass=Class'SmallRedeemerExplosion'
    DamageTypeClass=Class'DamTypeRedeemer'
}
