//==============================================================================
// UT2003FlakAmmoPickup.uc
// Flak Monkey!
// GreatEmerald, 2008
//==============================================================================

class UT2003FlakAmmoPickup extends FlakAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
   InventoryType=class'UT2003FlakAmmo'
}
