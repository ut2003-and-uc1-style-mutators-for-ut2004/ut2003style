//==============================================================================
// UT2003LinkAttachment.uc
// Link Me!
// 2008, GreatEmerald
//==============================================================================

class UT2003LinkAttachment extends LinkAttachment;

defaultproperties
{
  Mesh=mesh'Weapons.LinkGun_3rd'
  RelativeLocation=(X=0,Y=0,Z=0)
  RelativeRotation=(Pitch=0)
}
