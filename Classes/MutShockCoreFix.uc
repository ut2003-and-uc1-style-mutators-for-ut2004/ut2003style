//-----------------------------------------------------------------------------
// MutShockCoreFix.uc
// A mutator for making the Shock Cores appear normal sized
// Made by GreatEmerald, 2008
//-----------------------------------------------------------------------------

class MutShockCoreFix extends Mutator;

var bool bCheckShockAmmoUpdate, bOldShockUpdate;

replication{
    reliable if( Role==ROLE_Authority)
	     bCheckShockAmmoUpdate;
}


function bool CheckReplacement(Actor Other, out byte bSuperRelevant) //A non-ugly way of doing this - still unknown if works online
{
      //bSuperRelevant = 0;

      if (ShockAmmoPickup(Other) != None)
      {
            if (Other.PrePivot==(vect(0,0,32)))
                Other.PrePivot=(vect(0,0,22));
            Other.SetDrawScale(0.6);
            Other.SetDrawScale3D(vect(1,1,1));
            Class'ShockAmmoPickup'.default.bOnlyReplicateHidden=False;

            // meowcat: Now toggle the bool to let the clients know that they need to check for an update
            bCheckShockAmmoUpdate = !bCheckShockAmmoUpdate; // toggle it
       }
       return true;
}

// meowcat: this will be called client side....
simulated function PostNetReceive()
{
       local ShockAmmoPickup P;

       if(bCheckShockAmmoUpdate != bOldShockUpdate)
       {
            bOldShockUpdate = bCheckShockAmmoUpdate; // unset the trigger
            foreach DynamicActors(class'ShockAmmoPickup', P)
            {
                   P.SetDrawScale(0.6); // go ahead and do whatever other changes you want here as well
                   P.SetDrawScale3D(vect(1,1,1));
                   if (P.PrePivot==(vect(0,0,32)))
                       P.PrePivot=(vect(0,0,22));
            }
       }
       Super.PostNetReceive();
}


defaultproperties
{
    IconMaterialName="MutatorArt.nosym"
    FriendlyName="[UT2003 Style] Shock Core display fix"
    // other defaults etc.
    RemoteRole=ROLE_SimulatedProxy// client's copies of the actor can execute simulated functions
    bAlwaysRelevant=true // always keep this mutators replication channel open
    bNetNotify=true // call PostNetReceive whenever a replicated variable is received
    bAddToServerPackages=true
    Description="Fixes the display of the Shock Core (ammo pickup for the Shock Rifle).||This mutator will set the Shock Cores to have the dimensions (and therefore appearance) of Shock Cores of Unreal, Unreal Tournament, Unreal Tournament 2003 and Unreal Championship.|This mutator doesn't replace anything and therefore is compatibe with other mutators. It is a mutator of the UT2003 style fixes by GreatEmerald."
}
