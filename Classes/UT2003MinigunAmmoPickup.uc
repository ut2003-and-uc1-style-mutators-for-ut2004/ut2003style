//==============================================================================
// UT2003MinigunAmmoPickup.uc
// TBD
// GreatEmerald, 2008
//==============================================================================

class UT2003MinigunAmmoPickup extends MinigunAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
   InventoryType=class'UT2003MinigunAmmo'
}
