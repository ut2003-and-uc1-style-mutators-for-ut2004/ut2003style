//==============================================================================
// UT2003SniperRiflePickup.uc
// Sure, let's change this single setting in this class and overwrite it!
// GreatEmerald, 2008
//==============================================================================

class UT2003SniperRiflePickup extends SniperRiflePickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003SniperRifle'
}
