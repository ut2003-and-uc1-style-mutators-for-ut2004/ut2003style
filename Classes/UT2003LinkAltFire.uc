//==============================================================================
// UT2003LinkAltFire.uc
// Alt Fire was misnamed...
// GreatEmerald, 2008
//==============================================================================

class UT2003LinkAltFire extends LinkFire;

simulated function PostBeginPlay()
{
    if (LinkGun(Weapon) != None && !LinkGun(Weapon).bUseOldWeaponMesh) //GEm: Make sure we don't break the original mesh, but it shouldn't be default
        FireAnim = 'Idle';
    
    Super.PostBeginPlay();
}

function SetLinkTo(Pawn Other)
{
    if (LockedPawn != None && Weapon != None)
    {
        RemoveLink(1 + UT2003LinkGun(Weapon).Links, Instigator);
        UT2003LinkGun(Weapon).Linking = false;
    }

    LockedPawn = Other;

    if (LockedPawn != None)
    {
        if (!AddLink(1 + UT2003LinkGun(Weapon).Links, Instigator))
        {
            bFeedbackDeath = true;
        }
        UT2003LinkGun(Weapon).Linking = true;

        LockedPawn.PlaySound(MakeLinkSound, SLOT_Interact); //Maybe this will give better sounds
    }
}

defaultproperties
{
   AmmoClass=class'UT2003LinkAmmo'
   FireRate=0.2
   Damage=15
   MomentumTransfer=20000.0
   BeamSounds(0)=Sound'UT2003Weapons.LinkGun.BLinkGunBeam1'
   BeamSounds(1)=Sound'UT2003Weapons.LinkGun.BLinkGunBeam2'
   BeamSounds(2)=Sound'UT2003Weapons.LinkGun.BLinkGunBeam3'
   BeamSounds(3)=Sound'UT2003Weapons.LinkGun.BLinkGunBeam4'
   FireAnim="AltFire"
}
