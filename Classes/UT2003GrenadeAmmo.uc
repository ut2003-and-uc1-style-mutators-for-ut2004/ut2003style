//==============================================================================
// UT2003GrenadeAmmo.uc
// These go into Assault Rifles.
// GreatEmerald, 2008
//==============================================================================

class UT2003GrenadeAmmo extends GrenadeAmmo;

defaultproperties
{
    PickupClass=class'UT2003AssaultAmmoPickup'
}
