//==============================================================================
// UT2003ONSPainter.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003ONSPainter extends ONSPainter;

defaultproperties
{
    ItemName="UT2003 Target Painter"
    PickupClass=class'UT2003ONSPainterPickup'
}
