/*******************************************************************************
 * MutUT2003Extra.uc
 * Mutator for adding extra UT2003-styled things that don't relate to weapons,
 * so can be used with other Arena mutators
 * GreatEmerald, 2012
 ******************************************************************************/  

class MutUT2003Extra extends Mutator;

struct S_DeathSoundMap
{
    var Array<Sound> DeathSounds;
    var class<xPawnSoundGroup> SoundGroupClass;
};
var Array<S_DeathSoundMap> DeathSoundMaps;

var class<Actor> UT2003TransEffects[2], UT2003TransOutEffect[2];
var Sound UT2003RagImpactSounds[4];
var class<UT2003SoundGroupManager> SoundGroupManager;

var class<UT2003DeathPhraseRules> RulesClass;
var UT2003DeathPhraseRules Rules;

var class<UT2003RagImpactLRI> LRIClass;

replication
{
    unreliable if (Role == ROLE_Authority)
        RegisterRagdollSounds; //GEm: Send this to clients, hope they simulate
}

function bool CheckReplacement(Actor Other, out byte bSuperRelevant) //GEm: Usually would go for ModifyPlayer(), but it plays the spawn effect before executing that, so...
{
    local xPawn xP;
    
    
    if (xPawn(Other) != None)
    {
        xP = xPawn(Other);
        xP.TransEffects[0] = UT2003TransEffects[0]; //GEm: Argh for not being able to assign arrays directly. D could do that just fine!
        xP.TransEffects[1] = UT2003TransEffects[1];
        xP.TransOutEffect[0] = UT2003TransOutEffect[0];
        xP.TransOutEffect[1] = UT2003TransOutEffect[1];
        if (Level.NetMode == NM_Standalone)
            RegisterRagdollSounds(xP); //GEm: For clients we perform this on ModifyPlayer, as we need a working PRI
            
        RegisterDeathSounds(xP); //GEm: We need this so that once we override death sounds, we could know what to play
        RegisterGameRules();
    }
    return Super.CheckReplacement(Other, bSuperRelevant);
}

function ModifyPlayer(Pawn Other)
{
    if (xPawn(Other) != None && Level.NetMode != NM_Standalone)
        RegisterRagdollSoundsClient(xPawn(Other));
    
    Super.ModifyPlayer(Other);
}

function RegisterDeathSounds (xPawn xP)
{
    local int i;
    local S_DeathSoundMap DSM;
    
    if (xP.SoundGroupClass == None)
        return;
    
    for (i=0; i<DeathSoundMaps.length; i++)
    {
        if (xP.SoundGroupClass == DeathSoundMaps[i].SoundGroupClass)
            return;
    }
    DSM.SoundGroupClass = xP.SoundGroupClass;
    DSM.DeathSounds = xP.SoundGroupClass.default.DeathSounds;
    DeathSoundMaps[DeathSoundMaps.length] = DSM;
}

function RegisterRagdollSounds(xPawn xP)
{
    local int i;
    
    for (i=0; i<4; i++)
        xP.RagImpactSounds[i] = UT2003RagImpactSounds[i]; //GEm: Not sure if it works, as dynamic arrays have no clue about replication.
}

function RegisterRagdollSoundsClient(xPawn xP)
{
    local UT2003RagImpactLRI LRI;
    local LinkedReplicationInfo CRI;
    
    //log(Self@"RegisterRagdollSoundsClient!");
    if (xP.PlayerReplicationInfo == None //If pawn doesn't have a PRI or if we already got one, you see
        || (xP.PlayerReplicationInfo.CustomReplicationInfo != None && UT2003RagImpactLRI(xP.PlayerReplicationInfo.CustomReplicationInfo) != None) )
        return;
    
    //log(Self@"Attempting to spawn a LRI!");
    LRI = Spawn(LRIClass, xP);
    if (LRI != None)
    {
        //log(Self@"LRI spawned!");
        if (xP.PlayerReplicationInfo.CustomReplicationInfo == None)
            xP.PlayerReplicationInfo.CustomReplicationInfo = LRI;
        else
        {
            //log(Self@"Another LRI exists!");
            for (CRI=xP.PlayerReplicationInfo.CustomReplicationInfo; CRI != None; CRI=CRI.NextReplicationInfo)
            {
                if (CRI.NextReplicationInfo == None)
                {
                    CRI.NextReplicationInfo = LRI;
                    break;
                }
            }
        }
    }
}

function RegisterGameRules()
{
    if (Rules != None) //GEm: Already registered
        return;
    
    Rules = spawn(RulesClass, Self);
	if(Rules != None)
	{
    	if ( Level.Game.GameRulesModifiers == None )
            Level.Game.GameRulesModifiers = Rules;
    	else
            Level.Game.GameRulesModifiers.AddGameRules(Rules);
    }
}

defaultproperties
{
    IconMaterialName="MutatorArt.nosym"
    ConfigMenuClassName=""
    GroupName="UT2003Extra"
    FriendlyName="[UT2003 Style] Extra functionality"
    Description="Adds additional functionality that is not related to weapons, such as UT2003 teleportation effects, ragdoll sounds, etc."
    bAddToServerPackages=true
    
    UT2003TransOutEffect(0)=Class'UT2003TransDeres'
    UT2003TransOutEffect(1)=Class'UT2003TransDeresBlue'
    UT2003TransEffects(0)=Class'UT2003TransEffectRed'
    UT2003TransEffects(1)=Class'UT2003TransEffectBlue'
    UT2003RagImpactSounds(0)=Sound'UT2003GeneralImpacts.Wet.Breakbone_01'
    UT2003RagImpactSounds(1)=Sound'UT2003GeneralImpacts.Wet.Breakbone_02'
    UT2003RagImpactSounds(2)=Sound'UT2003GeneralImpacts.Wet.Breakbone_03'
    UT2003RagImpactSounds(3)=Sound'UT2003GeneralImpacts.Wet.Breakbone_04'
    SoundGroupManager=Class'UT2003SoundGroupManager'
    RulesClass=Class'UT2003DeathPhraseRules'
    LRIClass=Class'UT2003RagImpactLRI'
}
