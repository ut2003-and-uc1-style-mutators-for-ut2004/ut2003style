//==============================================================================
// UT2003BioRifle.uc
// Bio Hazard!
// 2008, GreatEmerald
//==============================================================================

class UT2003BioRifle extends BioRifle;

defaultproperties
{
    ItemName="UT2003 Bio Rifle"
    PickupClass=class'UT2003BioRiflePickup'
    FireModeClass(0)=UT2003BioFire
    FireModeClass(1)=UT2003BioChargedFire
    SelectSound=Sound'WeaponSounds.BioRifle.SwitchToBioRifle'
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=209,Y1=182,X2=289,Y2=242)
}
