/*******************************************************************************
 * UT2003TransDeresBlue.uc
 * To make things point upwards and fix typos.
 * GreatEmerald, 2012
 ******************************************************************************/   

class UT2003TransDeresBlue extends TranseDeResBlue;

simulated event PostBeginPlay()
{
    SetRotation(rot(0,0,0));
    Super.PostBeginPlay();
}

defaultproperties
{
}
 
