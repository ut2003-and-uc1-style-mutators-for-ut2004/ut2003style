//-----------------------------------------------------------------------------
// MutSJumpCombo.uc
// Adds the UT2003 SuperJump combo to the game.
// Made by Leo (T.C.K.)
//-----------------------------------------------------------------------------

class MutSJumpCombo extends Mutator;

var xPlayer NotifyPlayer[32];

function Timer()
{
	local int i;
	
	for ( i=0; i<32; i++ )
		if ( NotifyPlayer[i] != None )
		{
			NotifyPlayer[i].ClientReceiveCombo("UT2003Style.ComboSuperjump");
			NotifyPlayer[i] = None;
		}
}

function bool IsRelevant(Actor Other, out byte bSuperRelevant)
{
	local int i;
	
	if ( xPlayer(Other) != None )
	{
		for ( i=0; i<16; i++ )
		{
			if ( xPlayer(Other).ComboNameList[i] ~= "UT2003Style.ComboSuperjump" )
				break;
			else if ( xPlayer(Other).ComboNameList[i] == "" )
			{
				xPlayer(Other).ComboNameList[i] = "UT2003Style.ComboSuperjump";
				break;
			}
		}
		for ( i=0; i<32; i++ )
			if ( NotifyPlayer[i] == None )
			{
				NotifyPlayer[i] = xPlayer(Other);
				SetTimer(0.5, false);
				break;
			}
	}
	
	if ( NextMutator != None )
		return NextMutator.IsRelevant(Other, bSuperRelevant);
	else
		return true;
}

defaultproperties
{
     FriendlyName="[UT2003 Style] Super-Jump Combo"
     Description="Adds the Super-Jump combo (Forward-Back-Forward-Back).||This mutator was made by Leo (T.C.K.) based on the UT2003 Beta code.|It isn't fully supported by GreatEmerald."
}
