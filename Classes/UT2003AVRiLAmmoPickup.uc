//==============================================================================
// UT2003AVRiLAmmoPickup.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003AVRiLAmmoPickup extends ONSAVRiLAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003AVRiLAmmo'
    //PickupSound=Sound'WeaponSounds.BPickup2'
    //TransientSoundVolume=0.45
}
