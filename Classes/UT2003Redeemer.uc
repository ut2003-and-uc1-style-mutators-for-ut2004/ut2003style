//==============================================================================
// UT2003Redeemer.uc
// Not just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003Redeemer extends Redeemer;

defaultproperties
{
    ItemName="UT2003 Redeemer"
    PickupClass=class'UT2003RedeemerPickup'
    FireModeClass(1)=Class'UT2003RedeemerGuidedFire'
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=209,Y1=14,X2=289,Y2=54)
}
