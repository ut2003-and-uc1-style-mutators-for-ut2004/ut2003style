//-----------------------------------------------------------------------------
// SuperjumpThrus.uc
// The SuperJump thrust effect.
// Made by Leo (T.C.K.)
//-----------------------------------------------------------------------------
class SuperjumpThrust extends xEmitter;

simulated function PostBeginPlay()
{
    SetTimer(30.0, false);//Timer increased from 15
}

simulated function Timer()
{
    mRegen = false;
}

simulated function Tick(float dt)
{
    if (Owner != None && Owner.Physics == PHYS_Falling && Owner.Velocity.Z > 50)
    {
        mRegenRange[0] = 40.0;
        mRegenRange[1] = 40.0;
    }
    else
    {
        mRegenRange[0] = 0.0;
        mRegenRange[1] = 0.0;
    }
}

defaultproperties
{
     mStartParticles=0
     mMaxParticles=20
     mLifeRange(0)=0.750000
     mLifeRange(1)=0.750000
     mRegenRange(0)=0.000000
     mRegenRange(1)=0.000000
     mSpeedRange(0)=0.000000
     mSpeedRange(1)=0.000000
     mAirResistance=2.000000
     mOwnerVelocityFactor=0.650000
     mRandOrient=True
     mSpinRange(0)=-100.000000
     mSpinRange(1)=100.000000
     mGrowthRate=-10.000000
     mRandTextures=True
     mNumTileColumns=4
     mNumTileRows=4
     RemoteRole=ROLE_SimulatedProxy
     Skins(0)=Texture'XEffects.SmokeAlphab_t'
     Style=STY_Additive
}