//==============================================================================
// UT2003Translocator.uc
// Sick!
// GreatEmerald, 2008, 2009
//==============================================================================

class UT2003Translocator extends TransLauncher;

simulated event RenderOverlays( Canvas Canvas ) //Copied all of this ONLY to fix skins on the blue translocator!
{
    local float tileScaleX, tileScaleY, dist, clr;
    local float NewTranslocScale;

    if ( (PlayerController(Instigator.Controller) != None) && (PlayerController(Instigator.Controller).ViewTarget == TransBeacon) )
    {
        tileScaleX = Canvas.SizeX / 640.0f;
        tileScaleY = Canvas.SizeY / 480.0f;

        Canvas.DrawColor.R = 255;
        Canvas.DrawColor.G = 255;
        Canvas.DrawColor.B = 255;
        Canvas.DrawColor.A = 255;

        Canvas.Style = 255;
        Canvas.SetPos(0,0);
        Canvas.DrawTile( Material'TransCamFB', Canvas.SizeX, Canvas.SizeY, 0.0, 0.0, 512, 512 ); // !! hardcoded size
        Canvas.SetPos(0,0);

        if ( !Level.IsSoftwareRendering() )
        {
            dist = VSize(TransBeacon.Location - Instigator.Location);
            if ( dist > MaxCamDist )
            {
                clr = 255.0;
            }
            else
            {
                clr = (dist / MaxCamDist);
                clr *= 255.0;
            }
            clr = Clamp( clr, 20.0, 255.0 );
            Canvas.DrawColor.R = clr;
            Canvas.DrawColor.G = clr;
            Canvas.DrawColor.B = clr;
            Canvas.DrawColor.A = 255;
            Canvas.DrawTile( Material'ScreenNoiseFB', Canvas.SizeX, Canvas.SizeY, 0.0, 0.0, 512, 512 ); // !! hardcoded size
        }
    }
    else
    {
        if ( TransBeacon == None )
            NewTranslocScale = 1;
        else
            NewTranslocScale = 0;

        if ( NewTranslocScale != TranslocScale )
        {
            TranslocScale = NewTranslocScale;
            SetBoneScale(0,TranslocScale,'Beacon');
        }
        if ( TranslocScale != 0 )
        {
            TranslocRot.Yaw += 120000 * (Level.TimeSeconds - OldTime);
            OldTime = Level.TimeSeconds;
            SetBoneRotation('Beacon',TranslocRot,0);
        }
        if ( !bTeamSet && (Instigator.PlayerReplicationInfo != None) && (Instigator.PlayerReplicationInfo.Team != None) )
        {
            bTeamSet = true;
        }
        Super.RenderOverlays(Canvas);
    }
}


defaultproperties
{
    ItemName="UT2003 Translocator"
    //IconCoords=(X1=427,Y1=143,X2=492,Y2=169)

    //TranslocScale=+1.0
    AmmoChargeF=5.0f
    RepAmmo=5
    AmmoChargeMax=5.0f
    AmmoChargeRate=0.30f
    FireModeClass(0)=UT2003TransFire
    //InventoryGroup=10
    PickupClass=class'UT2003TransPickup'
    AttachmentClass=class'UT2003TransAttachment'

    SelectAnim=Pickup
    Skins(0)=Texture'WeaponSkins.Skins.TransLauncherTex0'
    Skins(1)=Shader'WeaponSkins.AmmoPickups.BioRifleGlassRef'
    Skins(2)=Texture'WeaponSkins.Skins.TransLauncherTex0'
    Skins(3)=Texture'WeaponSkins.Skins.TransLauncherTex0'

    OldMesh=mesh'Weapons.TransLauncher_1st'
    OldDrawScale=1.0
    OldPlayerViewOffset=(X=7,Y=7,Z=-4.5)
    OldSmallViewOffset=(X=19,Y=13,Z=-10.5)
    OldPlayerViewPivot=(Pitch=1000,Roll=0,Yaw=400)

    OldCenteredRoll=0
    OldPickup="WeaponStaticMesh.TranslocatorNEW"
    bUseOldWeaponMesh=True
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader' //GEm: Probably nobody is going to see this, oh well.
    IconCoords=(X1=209,Y1=4,X2=289,Y2=64)
}
