//==============================================================================
// UT2003ShockRifle.uc
// Shocking.
// 2008, GreatEmerald
//==============================================================================

class UT2003ShockRifle extends ShockRifle;

defaultproperties
{
    ItemName="UT2003 Shock Rifle"

    FireModeClass(0)=UT2003ShockFire
    FireModeClass(1)=UT2003ShockAltFire

    PickupClass=class'UT2003ShockRiflePickup'
    AttachmentClass=class'UT2003ShockAttachment'
    bUseOldWeaponMesh=True
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=209,Y1=133,X2=289,Y2=172)
}

