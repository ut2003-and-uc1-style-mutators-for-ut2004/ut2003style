//==============================================================================
// UT2003ShieldPack.uc
// Stone Skin!
// GreatEmerald, 2008
//==============================================================================

class UT2003ShieldPack extends ShieldPack;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

auto state Pickup
{   
    function Touch( actor Other )
    {
        local Pawn P;
            
        if ( ValidTouch(Other) ) 
        {           
            P = Pawn(Other);
            if ( P.AddShieldStrength(1) || !Level.Game.bTeamGame ) //GE: Add shields twice, so that it works around the "50 shields don't stack" hack in xPawn.
            {                                                      //GE: Hack to undo another hack!
                P.AddShieldStrength(ShieldAmount-1);
                AnnouncePickup(P);
                SetRespawn();
            }
        }
    }
}

defaultproperties
{
    PickupSound=sound'UT2003Weapons.Pickups.ShieldPack'
    TransientSoundVolume=0.45
}
