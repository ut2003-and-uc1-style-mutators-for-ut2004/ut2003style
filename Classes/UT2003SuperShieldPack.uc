//==============================================================================
// UT2003SuperShieldPack.uc
// Magic Mirror!
// GreatEmerald, 2008
//==============================================================================

class UT2003SuperShieldPack extends SuperShieldPack;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    PickupSound=sound'UT2003Weapons.Pickups.LargeShieldPickup'
    TransientSoundVolume=0.45
}
