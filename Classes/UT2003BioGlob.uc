//==============================================================================
// UT2003BioGlob.uc
// Bloblet!
// GreatEmerald, 2008
//==============================================================================

class UT2003BioGlob extends BioGlob;

function AdjustSpeed()
{
    Velocity = Vector(Rotation) * Speed;
    Velocity.Z += TossZ;
}

defaultproperties
{
    Damage=23.0 // full load = 135(?) damage
    ExplodeSound=Sound'UT2003Weapons.BioRifle.BioRifleGoo1'
    ImpactSound=Sound'UT2003Weapons.BioRifle.BioRifleGoo2'
}
