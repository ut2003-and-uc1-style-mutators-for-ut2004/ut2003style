//==============================================================================
// UT2003ShockRiflePickup.uc
// Sounds, but not here!
// 2008, GreatEmerald
//==============================================================================

class UT2003ShockRiflePickup extends ShockRiflePickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003ShockRifle'
    StaticMesh=StaticMesh'WeaponStaticMesh.ShockRiflePickup'
    DrawScale=0.5
    Skins[0]=WeaponSkins.Skins.ShockTex0
    Skins[1]=WeaponSkins.Skins.ShockTex0
    Standup=(Z=0.75,Y=0,X=0)
}

