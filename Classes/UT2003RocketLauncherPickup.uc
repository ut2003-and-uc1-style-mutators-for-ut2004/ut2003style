//==============================================================================
// UT2003RocketLauncherPickup.uc
// Rocket Scientist!
// GreatEmerald, 2008
//==============================================================================

class UT2003RocketLauncherPickup extends RocketLauncherPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

simulated event ClientTrigger()
{
    bHidden = true;
    if ( EffectIsRelevant(Location, false) && !Level.GetLocalPlayerController().BeyondViewDistance(Location, CullDistance)  )
        spawn(class'UT2003WeaponFadeEffect',self);
}

defaultproperties
{
    InventoryType=class'UT2003RocketLauncher'
}
