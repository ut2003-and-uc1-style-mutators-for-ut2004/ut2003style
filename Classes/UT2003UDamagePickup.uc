//==============================================================================
// UT2003UDamagePickup.uc
// This might be hard...
// GreatEmerald, 2008
//==============================================================================

class UT2003UDamagePickup extends UDamagePack;

var Sound UDamageFireSound;
var class<UDamageTimer> CustomTimer;

auto state Pickup
{
    function Touch( actor Other )
    {
        local xPawn P;
        local UDamageTimer UDT;

        if ( ValidTouch(Other) && xPawn(Other) != None )
        {
            P = xPawn(Other);
            P.UDamageSound = UDamageFireSound;
            UDT = Spawn(CustomTimer, Other);
            P.UDamageTimer = UDT;
            P.EnableUDamage(30);
            AnnouncePickup(Pawn(Other));
            SetRespawn();
        }
    }
}

defaultproperties
{
	//InventoryType       = class'UT2003UDamage'
	PickupSound=sound'UT2003Weapons.UDamage.UDamagePickUp'
    UDamageFireSound = Sound'UT2003Weapons.UDamage.UDamageFire'
    CustomTimer = Class'UT2003UDamageTimer'
}
