//==============================================================================
// UT2003AssaultRifle.uc
// Tarydium Arms.
// GreatEmerald, 2008
//==============================================================================

class UT2003AssaultRifle extends AssaultRifle;

function AttachToPawn(Pawn P)
{
    local name BoneName;

    if ( ThirdPersonActor == None )
    {
        ThirdPersonActor = Spawn(AttachmentClass,Owner);
        InventoryAttachment(ThirdPersonActor).InitFor(self);
    }
    BoneName = P.GetWeaponBoneFor(self);
    if ( BoneName == '' )
    {
        ThirdPersonActor.SetLocation(P.Location);
        ThirdPersonActor.SetBase(P);
    }
    else
        P.AttachToBone(ThirdPersonActor,BoneName);

    if ( bDualMode )
    {
        BoneName = P.GetOffHandBoneFor(self);
        if ( BoneName == '' )
            return;
        if ( OffhandActor == None )
        {
            OffhandActor = AssaultAttachment(Spawn(AttachmentClass,Owner));
            OffhandActor.InitFor(self);
        }
        P.AttachToBone(OffhandActor,BoneName);
        if ( OffhandActor.AttachmentBone == '' )
            OffhandActor.Destroy();
        else
        {
            ThirdPersonActor.SetDrawScale(0.3);
            OffhandActor.SetDrawScale(0.3);
            OffhandActor.bDualGun = true;
            OffhandActor.TwinGun = AssaultAttachment(ThirdPersonActor);
            if ( Mesh == OldMesh )
            {
                OffhandActor.SetRelativeRotation(rot(0,32768,0));
                OffhandActor.SetRelativeLocation(vect(20,-10,-5));
            }
            else
            {
                //OffhandActor.SetRelativeRotation(rot(0,0,32768));
                OffhandActor.SetRelativeLocation(vect(40,-3,-7));
            }
            AssaultAttachment(ThirdPersonActor).TwinGun = OffhandActor;
        }
    }
}

function bool HandlePickupQuery( pickup Item )
{
    if ( class == Item.InventoryType )
    {
        if ( bDualMode )
            return super(Weapon).HandlePickupQuery(Item);
        bDualMode = true;
        if ( Instigator.Weapon == self )
        {
            PlayOwnedSound(SelectSound, SLOT_Interact,,,,, false);
            AttachToPawn(Instigator);
        }
        if (Level.GRI.WeaponBerserk > 1.0)
            CheckSuperBerserk();
        else
            FireMode[0].FireRate = FireMode[0].Default.FireRate *  0.55;

        FireMode[0].Spread = FireMode[0].Default.Spread * 1.5;
        if (xPawn(Instigator) != None && xPawn(Instigator).bBerserk)
            StartBerserk();

        return false;
    }
    /*if ( item.inventorytype == AmmoClass[1] )
    {
        if ( (AmmoCharge[1] >= MaxAmmo(1)) && (AmmoCharge[0] >= MaxAmmo(0)) )
            return true;
        item.AnnouncePickup(Pawn(Owner));
        AddAmmo(50, 0);
        AddAmmo(Ammo(item).AmmoAmount, 1);
        item.SetRespawn();
        return true;
    }

    if ( Inventory == None )
        return false;

    return Inventory.HandlePickupQuery(Item);*/

    /*local WeaponPickup wpu;
    local int i;

    if ( bNoAmmoInstances )
    {
        // handle ammo pickups
        for ( i=0; i<2; i++ )
        {
            if ( (item.inventorytype == AmmoClass[i]) && (AmmoClass[i] != None) )
            {
                if ( AmmoCharge[i] >= MaxAmmo(i) )
                    return true;
                item.AnnouncePickup(Pawn(Owner));
                AddAmmo(Ammo(item).AmmoAmount, i);
                item.SetRespawn();
                return true;
            }
        }
    }

    if (class == Item.InventoryType)
    {
        wpu = WeaponPickup(Item);
        if (wpu != None)
            return !wpu.AllowRepeatPickup();
        else
            return false;
    }

    if ( Inventory == None )
        return false;

    return Inventory.HandlePickupQuery(Item);*/
    Super(Weapon).HandlePickupQuery(Item);
    return Inventory.HandlePickupQuery(Item);

}

/*simulated function int MaxAmmo(int mode)
{
    if ( bDualMode )
        return 2 * FireMode[mode].AmmoClass.Default.MaxAmmo;
    else
        return FireMode[mode].AmmoClass.Default.MaxAmmo;
}*/

/*simulated function MaxOutAmmo()
{
    if ( AmmoClass[1] != None )
      AmmoCharge[1] = MaxAmmo(1);

    if ( Ammo[0] != None )
        Ammo[0].AmmoAmount = Ammo[0].MaxAmmo;
    if ( Ammo[1] != None )
        Ammo[1].AmmoAmount = Ammo[1].MaxAmmo;
} */


defaultproperties
{
    ItemName="UT2003 Assault Rifle"
    FireModeClass(0)=UT2003AssaultFire
    FireModeClass(1)=UT2003AssaultGrenade
    PickupClass=class'UT2003AssaultRiflePickup'
    AttachmentClass=class'UT2003AssaultAttachment'
    bUseOldWeaponMesh=True
    bNoAmmoInstances=False
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=130,Y1=71,X2=209,Y2=117)
}
