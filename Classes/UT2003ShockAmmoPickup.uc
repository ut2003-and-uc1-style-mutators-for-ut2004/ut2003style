//==============================================================================
// UT2003ShockAmmoPickup.uc
// Shiny!
// 2008, GreatEmerald
//==============================================================================

class UT2003ShockAmmoPickup extends ShockAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    InventoryType=class'UT2003ShockAmmo'
    DrawScale3D=(X=1,y=1,Z=1)
    DrawScale=0.6
    PrePivot=(Z=22)
}
