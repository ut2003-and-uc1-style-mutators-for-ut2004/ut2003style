//==============================================================================
// UT2003FlakAltFire.uc
// Flak Monkey!
// GreatEmerald, 2008
//==============================================================================

class UT2003FlakAltFire extends FlakAltFire;

defaultproperties
{
    AmmoClass=class'UT2003FlakAmmo'
    FireAnimRate=1.0

    FireSound=Sound'UT2003Weapons.FlakCannon.FlakCannonAltFire'

    FireRate=1.0
}

