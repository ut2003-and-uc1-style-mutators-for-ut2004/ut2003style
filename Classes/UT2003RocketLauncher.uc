//==============================================================================
// UT2003RocketLauncher.uc
// Rocket Scientist!
// GreatEmerald, 2008
//==============================================================================

class UT2003RocketLauncher extends RocketLauncher;

defaultproperties
{
    ItemName="UT2003 Rocket Launcher"
    FireModeClass(0)=UT2003RocketFire
    FireModeClass(1)=UT2003RocketMultiFire
    PickupClass=class'UT2003RocketLauncherPickup'
    LockRequiredTime=1.4
    IconMaterial=Material'UT2003InterfaceContent.Hud.SkinAShader'
    IconCoords=(X1=484,Y1=14,X2=563,Y2=44)
}
