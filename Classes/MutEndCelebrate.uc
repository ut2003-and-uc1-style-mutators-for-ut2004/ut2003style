//==============================================================================
// MutEndCelebrate.uc
// If this works out, I'll celebrate.
// 2008, GreatEmerald
//==============================================================================

class MutEndCelebrate extends Mutator;

var CelebrationRules GRules;

function PostBeginPlay()
{
Super.PostBeginPlay();

	GRules = spawn(class'CelebrationRules');
	if(GRules != None)
	{
    //log("Celebration Rules - spawned the rules!");
    	if ( Level.Game.GameRulesModifiers == None ){
    		//log("Celebration Rules - registered the rules!");
            Level.Game.GameRulesModifiers = GRules;
    	}
    	else {
    		//log("Celebration Rules - registered the rules!");
            Level.Game.GameRulesModifiers.AddGameRules(GRules);
   		}
    }
}

defaultproperties
{
    IconMaterialName="MutatorArt.nosym"
    ConfigMenuClassName=""
    GroupName="Celebrate"
    FriendlyName="[UT2003 Style] Winner taunts"
    Description="Bots do a random taunt if they win the match."
    bAddToServerPackages=true
}
