/*******************************************************************************
 * UT2003RedeemerWarhead.uc
 * Does it still work with the new drawing system, I wonder.
 * GreatEmerald, 2012
 ******************************************************************************/

class UT2003RedeemerWarhead extends RedeemerWarhead;

#EXEC OBJ LOAD FILE=XGameShaders.utx

simulated function DrawHUD(Canvas Canvas)
{
    Canvas.Style = 255;
    Canvas.SetPos(0,0);
    Canvas.DrawColor = class'Canvas'.static.MakeColor(255,255,255);
    if ( bStaticScreen )
        Canvas.DrawTile( Material'ScreenNoiseFB', Canvas.SizeX, Canvas.SizeY, 0.0, 0.0, 512, 512 );
    else
    {	
        Canvas.DrawTile( Material'XGameShaders.RedeemerReticle', 0.5 * Canvas.SizeX, 0.5 * Canvas.SizeY, 0, 0, 512, 512 ); 
        Canvas.SetPos(0.5*Canvas.SizeX,0);
        Canvas.DrawTile( Material'XGameShaders.RedeemerReticle', 0.5 * Canvas.SizeX, 0.5 * Canvas.SizeY, 512, 0, -512, 512 ); 
        Canvas.SetPos(0,0.5*Canvas.SizeY);
        Canvas.DrawTile( Material'XGameShaders.RedeemerReticle', 0.5 * Canvas.SizeX, 0.5 * Canvas.SizeY, 0, 512, 512, -512 ); 
        Canvas.SetPos(0.5*Canvas.SizeX,0.5*Canvas.SizeY);
        Canvas.DrawTile( Material'XGameShaders.RedeemerReticle', 0.5 * Canvas.SizeX, 0.5 * Canvas.SizeY, 512, 512, -512, -512 ); 
    }	
}

defaultproperties
{
}
