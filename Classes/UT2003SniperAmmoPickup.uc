//==============================================================================
// UT2003SniperAmmoPickup.uc
// Head shot!
// GreatEmerald, 2008
//==============================================================================

class UT2003SniperAmmoPickup extends SniperAmmoPickup;

function RespawnEffect()
{
    spawn(class'UT2003SpawnEffect');
}

defaultproperties
{
    PickupMessage="You picked up sniper ammo."
    InventoryType=class'UT2003SniperAmmo'
}
