//==============================================================================
// UT2003BioFire.uc
// Blob!
// GreatEmerald, 2008
//==============================================================================

class UT2003BioFire extends BioFire;

defaultproperties
{
    AmmoClass=class'UT2003BioAmmo'
    ProjectileClass=class'UT2003BioGlob'
    FireSound=Sound'UT2003Weapons.BioRifle.BioRifleFire'
}
