//==============================================================================
// UT2003MineLayer.uc
// Just for the effects!
// GreatEmerald, 2008
//==============================================================================

class UT2003MineLayer extends ONSMineLayer;

defaultproperties
{
    ItemName="UT2003 Mine Layer"
    FireModeClass(0)=UT2003MineThrowFire
    PickupClass=class'UT2003MineLayerPickup'
    //SelectSound=Sound'WeaponSounds.BImpactHammerFire'
    //TransientSoundVolume=0.6
}
